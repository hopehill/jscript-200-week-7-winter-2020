const selectEl = document.getElementById('contact-kind');

const setSelectValidity = function() {
  if (selectEl.value === 'choose') {
  		selectEl.setCustomValidity("Must select an option");
	} else {
		selectEl.setCustomValidity('');
	}
}

selectEl.setCustomValidity('');

if (selectEl.value === 'business') {
	document.QuerySelector('.business').classList.remove('hide');
	document.QuerySelector('.technical').classList.add('hide');
	businessEl.required = true;
	techEl.required = false;
} else {
	document.QuerySelector('.business').classList.add('hide');
	document.QuerySelector('.technical').classList.remove('hide');
	businessEl.required = false;
	techEl.required = true;
}
};

setSelectValidity();
selectEl.addEventListener('change', setSelectValidity);

const form = document.getElementById('connect-form');
form.addEventListener('submit', function(e) {
	const inputs = document.getElementsByClassName('validate-input');
	let forIsValid = true;

	Array.from(inputs).forEach(input => {
	if (!input.checkValidity()) {
	formIsValid = false;
	const small = input.parentElement.querySelector('small').innerText = 
	input.ValidationMessage;
	}
});

	if(!formIsValid) {
		e.preventDefault();
	}
});
