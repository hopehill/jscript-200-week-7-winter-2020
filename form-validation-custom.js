const selectEl = document.getElementByID('contact-kind');

const setSelectValidity = function() {
	if(selectEl.value === 'choose') {
		selectEl.setCustomValidity('Must select an option');
	} else {
		selectEl.setCustomValidity('');
	}
};

setSelectValidity();
selectEl.addEventListener('change', setSelectValidity);

const form =document.getElementById('connect-form');
form.addEventListener('submit', function(e) {
	const inputs = document.getElementsByClassName('validate-input');
	let formValid = true;
	
	Array.from(inputs).forEach(input => {
		const inputs = input.parentElement.querySelector('smail');
		if (!input.checkValidate()) {
			formValid = false;
			const small.innerText = input.validationMessage;
			input.classList.add('invalid');
		} else {
			small.innerText = '';
			input.classList.remove('invalid');
	}
	});
		
	if(!formIsValid) {
		e.preventDefault();
}
});
